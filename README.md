# TwinCATdpr XAE

TwinCAT for Cable-Driven Parallel Robots.

This project consists of multiple submodules for separation of concerns and keeping code changes isolated in sub projects.

## Disclaimer

Make sure to read the official TwinCAT 3 Manual on Source Control before proceeding: https://download.beckhoff.com/download/document/automation/twincat3/TC3_SourceControl_EN.pdf

Pay close attention to page 7 (*Independent Project File*) and update your XAE Environment settings accordingly.

## Installation

While split into many repositories, all code you need is readily available through one main repository that you just have to clone using the following command

```bash
$ git clone --recurse-submodules https://gitlab.com/cable-robots/twincatdpr/xae.git
```

After that, go inside directory `XAE` and open `TwinCATdpr.sln` in VS 2017. Before performing any other task, make sure to import the `TwinCATdpr`-specific TwinCAT editor settings from file `twincatdpr.vssettings` into VS by going to `Tools / Import and Export Settings / Import Selected Environment Settings` and follow the modal dialog.

## Submodules

This project consists of the following sub-projects organized in submodules

* [PLC/PLCdpr](https://gitlab.com/cable-robots/twincatdpr/plc-plcdpr): main PLC for advanced control of cable robots
* [PLC/HMI](https://gitlab.com/cable-robots/twincatdpr/plc-hmi): interfacing with the standalone TwinCAT CNC HMI
* [CPP/StandardKinematicsTransformation](https://gitlab.com/cable-robots/twincatdpr/cpp-standard-kinematics-transformation): kinematic transformation (motion module) using the standard kinematics algorithm
* [CPP/PulleyKinematicsTransformation](https://gitlab.com/cable-robots/twincatdpr/cpp-pulley-kinematics-transformation): kinematic transformation motion module using the pulley-based kinematics algorithm

## Authors

* Philipp Tempel <p.tempel@tudelft.nl> https://philipptempel.me
